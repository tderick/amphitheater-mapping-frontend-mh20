import React from 'react'
import NavBar from './NavBar'
import Footer from './Footer.jsx'
import ClassRoom from './forms/ClassRoom'




//Creation du composant general de l'application

class App extends React.Component{
    render()
    {
      // alert('Bonjour de render')
      return(
        <header id="header-id">
            <NavBar />
            <ClassRoom/>
            <Footer/>
      </header>
      )
    }
  }

  export default App