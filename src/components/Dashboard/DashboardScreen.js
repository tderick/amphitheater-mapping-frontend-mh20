import React, { useState } from 'react'
import AdminNavbar from './AdminNavbar'
import AdminSideBar from './AdminSideBar'
import ModuleCard from './ModuleCard'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import '../../css/dashbord/dashboard.css'
import ProgramForm from '../forms/ProgramForm'

import Institution from '../forms/InstitutionForm'
import InstitutionForm from '../forms/InstitutionForm'
import DepartmentForm from '../forms/DepartmentForm'
import FacultyForm from '../forms/FacultyForm'
import LevelForm from '../forms/LevelForm'

import SpecialityForm from '../forms/SpecialityForm'
import ClassRoomForm from '../forms/ClassRoomForm'
import UserForm from '../forms/UserForm'
import ClassRoom from '../forms/ClassRoom'


const DashboardScreen = () => {

    const [menuActive, setMenuActive] = useState(true);

    return (

        <div className={(!menuActive) ? "d-flex toggled" : "d-flex"} id="wrapper">

            <AdminSideBar />

            <div id="page-content-wrapper">

                <AdminNavbar action={() => {
                    setMenuActive(menuActive => !menuActive);
                }} />

                <div className="container-fluid">

                    <div className="row" style={{ justifyContent: "center" }}>

                        <Switch>
                            <Route path="/add-user" component={UserForm}></Route>
                        </Switch>
                        <Switch>
                            <Route path="/add-institution" component={InstitutionForm}></Route>
                        </Switch>
                        <Switch>
                            <Route path="/add-department" component={DepartmentForm}></Route>
                        </Switch>
                        <Switch>
                            <Route path="/add-faculty" component={FacultyForm}></Route>
                        </Switch>
                        <Switch>
                            <Route path="/add-level" component={LevelForm}></Route>
                        </Switch>
                        <Switch>
                            <Route path="/add-program" component={ProgramForm}></Route>
                        </Switch>
                        <Switch>
                            <Route path="/add-speciality" component={SpecialityForm}></Route>
                        </Switch>
                        <Switch>
                            <Route exact path="/classrooms" component={ClassRoom}></Route>
                            <Route path="/classrooms/:query" component={ClassRoom}></Route>
                        </Switch>

                    </div>

                </div>

            </div>

        </div>
    )
}

export default DashboardScreen
