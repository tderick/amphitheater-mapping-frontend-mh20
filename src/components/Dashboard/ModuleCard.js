import React from 'react'

const ModuleCard = ({ title, icon }) => {
    return (
        <div class="col-lg-4 col-sm-6 col-xs-12">
            <div class="module-item">
                <div class="icone">
                    <i class={"fa fa-" + icon}></i>
                </div>
                <div class="titre">{title}</div>
            </div>
        </div>
    )
}

export default ModuleCard
