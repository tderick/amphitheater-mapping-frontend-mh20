import React from 'react'
import { Link } from 'react-router-dom'
import logo from './../../images/global/logo.jpg'

const AdminSideBar = () => {
    return (
        <div className="bg-bleu-light border-right bg-bleu-light" id="sidebar-wrapper">
            <div className="sidebar-heading">
                <img src={logo} style={{ maxWidth: "180px" }} />
            </div>
            <ul className="list-group list-group-flush">
                <li className="list-group-item list-group-item-action bg-bleu-light">
                    <a href="#"> <i className="fa fa-dashboard"></i> Dashboard</a>
                </li>
                 <li className="list-group-item list-group-item-action bg-bleu-light dropdown" data-target="#institution-th-list" data-toggle="collapse">
                    <a href="#" type="button" id="menu1"  > <i className="fa fa-university"></i>Institution
                        <span className="caret"></span>
                    </a>
                    <ul id="institution-th-list" className="collapse list-group list-group-flush">
                        <li className="collapse-list-item"><Link to="/add-institution"><i className="fa fa-plus-circle"></i> Add Faculty</Link> </li>
                          <li className="collapse-list-item"><Link to="/add-institution"><i className="fa fa-trash"></i> Remove</Link> </li>
                        <li className="divider"></li>
                    </ul>
                </li>
                <li className="list-group-item list-group-item-action bg-bleu-light dropdown" data-target="#user-th-list" data-toggle="collapse">
                    <a href="#" type="button" id="menu1"  > <i className="fa fa-user"></i>User
                        <span className="caret"></span>
                    </a>
                    <ul id="user-th-list" className="collapse list-group list-group-flush">
                        <li className="collapse-list-item"><Link to="/add-user"><i className="fa fa-plus-circle"></i> Add User</Link> </li>
                          <li className="collapse-list-item"><Link to="/add-user"><i className="fa fa-trash"></i> Remove</Link> </li>
                        <li className="divider"></li>
                    </ul>
                </li>
              <li className="list-group-item list-group-item-action bg-bleu-light dropdown" data-target="#faculty-th-list" data-toggle="collapse">
                    <a href="#" type="button" id="menu1"  > <i className="fa fa-graduation-cap"></i>Faculty
                        <span className="caret"></span>
                    </a>
                    <ul id="faculty-th-list" className="collapse list-group list-group-flush">
                        <li className="collapse-list-item"><Link to="/add-faculty"><i className="fa fa-plus-circle"></i> Add Faculty</Link> </li>
                          <li className="collapse-list-item"><Link to="/add-faculty"><i className="fa fa-trash"></i> Remove</Link> </li>
                        <li className="divider"></li>
                    </ul>
                </li>
                <li className="list-group-item list-group-item-action bg-bleu-light dropdown" data-target="#level-th-list" data-toggle="collapse">
                    <a href="#" type="button" id="menu1"  ><i className="fa fa-leanpub"></i> Level
                        <span className="caret"></span>
                    </a>
                    <ul id="level-th-list" className="collapse list-group list-group-flush">
                        <li className="collapse-list-item"><Link to="/add-level"><i className="fa fa-plus-circle"></i> Add Level</Link> </li>
                          <li className="collapse-list-item"><Link to="/add-level"><i className="fa fa-trash"></i> Remove</Link> </li>
                        <li className="divider"></li>
                    </ul>
                </li>
                <li className="list-group-item list-group-item-action bg-bleu-light dropdown" data-target="#speciality-th-list" data-toggle="collapse">
                    <a href="#" type="button" id="menu1"  ><i className="fa fa-strikethrough"></i>Specialitty
                        <span className="caret"></span>
                    </a>
                    <ul id="speciality-th-list" className="collapse list-group list-group-flush">
                        <li className="collapse-list-item"><Link to="/add-speciality"><i className="fa fa-plus-circle"></i> Add Speciality</Link> </li>
                          <li className="collapse-list-item"><Link to="/add-speciality"><i className="fa fa-trash"></i> Remove</Link> </li>
                        <li className="divider"></li>
                    </ul>
                </li>
                <li className="list-group-item list-group-item-action bg-bleu-light dropdown" data-target="#department-th-list" data-toggle="collapse">
                    <a href="#" type="button" id="menu1"  ><i className=" fa fa-building"></i> Campus
                        <span className="caret"></span>
                    </a>
                    <ul id="campus-th-list" className="collapse list-group list-group-flush">
                        <li className="collapse-list-item"><Link to="/add-campus"><i className="fa fa-plus-circle"></i> Add Campus</Link> </li>
                          <li className="collapse-list-item"><Link to="/add-campus"><i className="fa fa-trash"></i> Remove</Link> </li>
                        <li className="divider"></li>
                    </ul>
                </li>
                <li className="list-group-item list-group-item-action bg-bleu-light dropdown" data-target="#campus-th-list" data-toggle="collapse">
                    <a href="#" type="button" id="menu1"  ><i className=" fa fa-building"></i> Department
                        <span className="caret"></span>
                    </a>
                    <ul id="department-th-list" className="collapse list-group list-group-flush">
                        <li className="collapse-list-item"><Link to="/add-department"><i className="fa fa-plus-circle"></i> Add Department</Link> </li>
                          <li className="collapse-list-item"><Link to="/add-department"><i className="fa fa-trash"></i> Remove</Link> </li>
                        <li className="divider"></li>
                    </ul>
                </li>
                  <li className="list-group-item list-group-item-action bg-bleu-light dropdown" data-target="#classRoom-th-list" data-toggle="collapse">
                    <a href="#" type="button" id="menu1"  ><i className="fa fa-home"></i> Class Room
                        <span className="caret"></span>
                    </a>
                    <ul id="classRoom-th-list" className="collapse list-group list-group-flush">
                        <li className="collapse-list-item"><Link to="/add-classRoom"><i className="fa fa-plus-circle"></i> Add Class Room</Link> </li>
                          <li className="collapse-list-item"><Link to="/add-classRoom"><i className="fa fa-trash"></i> Remove</Link> </li>
                        <li className="divider"></li>
                    </ul>
                </li>
                 <li className="list-group-item list-group-item-action bg-bleu-light dropdown" data-target="#program-th-list" data-toggle="collapse">
                    <a href="#" type="button" id="menu1"  >Program
                        <span className="caret"></span>
                    </a>
                    <ul id="program-th-list" className="collapse list-group list-group-flush">
                        <li className="collapse-list-item"><Link to="/add-program"><i className="fa fa-plus-circle"></i> Add Program</Link> </li>
                          <li className="collapse-list-item"><Link to="/add-program"><i className="fa fa-trash"></i> Remove</Link> </li>
                        <li className="divider"></li>
                    </ul>
                </li>
                 <li className="list-group-item list-group-item-action bg-bleu-light dropdown" data-target="#delegate-th-list" data-toggle="collapse">
                    <a href="#" type="button" id="menu1"  ><i className="fa fa-users"></i> Delegate
                        <span className="caret"></span>
                    </a>
                    <ul id="delegate-th-list" className="collapse list-group list-group-flush">
                        <li className="collapse-list-item"><Link to="/add-delegate"><i className="fa fa-plus-circle"></i> Add Program</Link> </li>
                          <li className="collapse-list-item"><Link to="/add-delegate"><i className="fa fa-trash"></i> Remove</Link> </li>
                        <li className="divider"></li>
                    </ul>
                </li>
                <li className="list-group-item list-group-item-action bg-bleu-light">
                    <a href="#">Status</a>
                </li>
            </ul>
        </div>
    )
}

export default AdminSideBar
