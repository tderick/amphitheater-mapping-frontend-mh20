import React, { useRef } from 'react'
import { Link } from 'react-router-dom';

const AdminNavbar = ({ action }) => {

    const buttonToggleRef = useRef(null);
    const searchInputRef = useRef(null);

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-bleu-light border-bottom">
            <button
                ref={buttonToggleRef}
                onClick={action}
                className="btn btn-primary"
                id="th-bar"
            >
                <i className="fa fa-bars"></i>
            </button>

            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">

                <ul className="navbar-nav ml-auto mt-2 mt-lg-0">

                    <div className="form-inline" style={{ display: "flex" }}>
                        <div className="form-group mb-2">

                            <input ref={searchInputRef} type="search" className="form-control-plaintext" id="item" placeholder="item" style={{ backgroundColor: "white", borderRadius: 10, padding: 5 }} />

                        </div>
                        <div className="form-group mx-sm-3 mb-2">

                            <select className="form-control" id="label" placeholder="Standard">
                                <option value="room">Room</option>
                                <option value="faculty">Faculty</option>
                                <option value="campus">campus</option>
                            </select>
                        </div>
                        {/* <button type="submit" className="" id="go">Go</button> */}
                    </div>
                    <li className="nav-item active">
                        {/* <Link className="nav-link btn btn-outline-primary" href="/">Accueil</Link> */}
                    </li>
                    <li className="nav-item active">
                        {/* <Link className="nav-link" href="/"><i class="fa fa-sign-out"></i> </Link> */}
                        <i className="fa fa-search"
                            onClick={() => {
                                window.location.href = "/classrooms/" + searchInputRef.current.value;
                            }}
                        ></i>
                    </li>

                </ul>
            </div>
        </nav>
    )
}

export default AdminNavbar
