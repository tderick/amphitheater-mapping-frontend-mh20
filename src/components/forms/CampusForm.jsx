import React, {useState, useEffect} from 'react'
import './../../css/style.css'
import './../../fonts/linearicons/style.css'
import img1 from './../../images/image-1.png'
import img2 from './../../images/image-2.png'
import axios from 'axios'


const CampusForm = () => {
  
    const [institution, setInstitution]= useState([])
    const [campus, setCampus] = useState({
        name:"",
        institution:""
    })

    useEffect(() => {
       axios.get("https://localhost:8000/api/institutions").then(response =>{ setInstitution(response.data['hydra:member']) }).catch(error=>console.log(error.response));
    }, []);

    const handleChange = ({currentTarget})=>{
        const {name,value} = currentTarget;
        setCampus({...campus,[name]:value});
    }

    const handleSave = (e)=>{
        e.preventDefault();
        axios.post("https://localhost:8000/api/campuses", campus).then(response =>{ console.log(response.data['hydra:member']) }).catch(error=>console.log(error.response));
    }

    console.log(campus);
    return (
        <div class="wrapper">
        <div class="inner">
            <img src={img1} class="image-1" />
            <form action="" onSubmit={handleSave}>
                <h3>Campus Description</h3>

                <div class="form-holder">
                    <span class="lnr lnr-graduation-hat"></span>
                    <input type="text" class="form-control" placeholder="Campus Name" name="name" onChange={handleChange}/>
                </div>


                <div class="form-holder">
                    <span class="lnr lnr-list"></span>
                    <select class="browser-default custom-select" onChange={handleChange} name="institution">
                        <option selected>  University Membership</option>
                        {institution.map(institut => <option value={institut["@id"]} key={institut.id} >{institut.name}</option>
                        )}
                    </select>
                </div>
                <button type="submit">
                    <span>Register</span>
                </button>
            </form>
            <img src={img2} alt="" class="image-2" />
        </div>
        
    </div>
    )
}

export default CampusForm


  