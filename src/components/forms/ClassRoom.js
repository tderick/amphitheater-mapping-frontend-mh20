import Axios from 'axios';
import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import ClassCard from '../class/ClassCard';
import './../../css/classRoom/classRoom.css'


const ClassRoom = () => {

    const { query } = useParams();

    const [classrooms, setClassrooms] = useState([]);

    useEffect(() => {
        Axios.get("http://localhost:8000/api/classrooms").then(response => {
            // console.log(response);
            if (query) {
                alert(query);
                let temp = response.data['hydra:member'];
                setClassrooms(temp.filter(salle => query.toLowerCase().indexOf(salle.name.toLowerCase()) != -1));
                console.log(temp);
            } else {
                setClassrooms(response.data['hydra:member']);
            }
        }).catch(error => console.log(error.response));
    }, []);

    return (
        <div className="totalAllClass col-xs-12">
            <div className="row pousse">
                {classrooms.map(classroom => <ClassCard classe={classroom} />)}
            </div>
        </div>
    )
}
export default ClassRoom;
