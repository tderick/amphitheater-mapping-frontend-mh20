import React from 'react'

const ProgramLine = () => {
    return (
        <>
            <tr id="first" className="col-12">
                <td className="col-3">

                    <select name="days" className="form-control day" >
                        <option value="monday" className="">Monday</option>
                        <option value="tuesday" className="">Tuesday</option>
                        <option value="wednesday" className="">Wednesday</option>
                        <option value="thursday" className="">Thursday</option>
                        <option value="friday" className="">Friday</option>
                        <option value="saturday" className="">Saturday</option>
                        <option value="sunday" className="">Sunday</option>
                    </select>

                </td>

                <td className="col-4">

                    <input type="text" name="course" className="form-control course" placeholder="INF232" />


                </td>

                <td className="col-1">

                    <input type="time" name="beginingHour" placeholder="" className="form-control beginingHour" />

                </td>

                <td className="col-1">

                    <input type="time" name="endingHour" placeholder="10h00" className="form-control endingHour" />
                </td>

                <td className="col-3">

                    <select name="room" className="form-control selectedRoom">


                        <option value="<?php echo $room->getRoomId();?>">

                        </option>


                    </select>


                </td>
            </tr>
        </>
    )
}
export default ProgramLine