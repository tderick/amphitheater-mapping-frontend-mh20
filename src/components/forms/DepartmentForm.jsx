import Axios from "axios";
import React, { useState, useEffect } from "react";
import "./../../css/style.css";
import "./../../fonts/linearicons/style.css";
import img1 from "./../../images/image-1.png";
import img2 from "./../../images/image-2.png";

const DepartmentForm = () => {
  const [faculties, setFaculties] = useState([]);
  const [department, setDepartment] = useState({
    name: "",
    headname: "",
    faculty: "",
  });

  useEffect(() => {
    Axios.get("http://localhost:8000/api/faculties")
      .then((response) => {
        setFaculties(response.data["hydra:member"]);
      })
      .catch((error) => console.log(error.response));
  }, []);

  const handleChange = ({ currentTarget }) => {
    const { name, value } = currentTarget;
    setDepartment({ ...department, [name]: value });
    console.log(department);
  };

  const handleSave = (e) => {
    // e.preventDefault();
    Axios.post("http://localhost:8000/api/departments", department)
      .then((response) => {
        console.log(response.data["hydra:member"]);
      })
      .catch((error) => console.log(error.response));
  };

  return (
    <div class="wrapper">
      <div class="inner">
        <img src={img1} class="image-1" />
        <form onSubmit={handleSave}>
          <h3>Department</h3>

          <div class="form-holder">
            <span class="lnr lnr-graduation-hat"></span>
            <input
              type="text"
              class="form-control"
              placeholder="Chef de département"
              onChange={handleChange}
              name="headname"
            />
          </div>

          <div class="form-holder">
            <span class="lnr lnr-graduation-hat"></span>
            <input
              type="text"
              class="form-control"
              placeholder="Department"
              onChange={handleChange}
              name="name"
            />
          </div>

          <div class="form-holder">
            <span class="lnr lnr-graduation-hat"></span>
            <select
              name="faculty"
              id="faculty"
              className="form-control"
              onChange={handleChange}
            >
              {faculties.map((fac) => (
                <option value={fac["@id"]} key={fac.id}>
                  {fac.name}
                </option>
              ))}
            </select>
          </div>

          <button>
            <span>Register</span>
          </button>
        </form>
        <img src={img2} alt="" class="image-2" />
      </div>
    </div>
  );
};

export default DepartmentForm;
