import React,{useState, useEffect} from 'react'
import './../../css/style.css'
import './../../fonts/linearicons/style.css'
import img1 from './../../images/image-1.png'
import img2 from './../../images/image-2.png'
import axios from "axios"

const LevelForm = ()=>{

	const [specialities, setSpecialities]= useState([])
    const [level, setLevel] = useState({
        name:"",
        speciality:""
    })

    useEffect(() => {
       axios.get("https://localhost:8000/api/specialities").then(response =>{ setSpecialities(response.data['hydra:member']) }).catch(error=>console.log(error.response));
    }, []);

    const handleChange = ({currentTarget})=>{
        const {name,value} = currentTarget;
        setLevel({...level,[name]:value});
    }

    const handleSave = (e)=>{
        e.preventDefault();
        axios.post("https://localhost:8000/api/levels", level).then(response =>{ console.log(response.data['hydra:member']) }).catch(error=>console.log(error.response));
    }

    return(
        <div class="wrapper">
			<div class="inner">
                <img src={img1} class="image-1" />
				<form action="" onSubmit={handleSave}>
					<h3>Level Information</h3>

                
					<div class="form-holder">
						<span class="lnr lnr-user"></span>
						<input type="text" class="form-control" placeholder="Name" name="name" onChange={handleChange}/>
					</div>

                    <div class="form-holder">
						<span class="lnr lnr-graduation-hat"></span>
						<select class="browser-default custom-select" onChange={handleChange} name="speciality">
                            <option selected>  Speciality</option>
	{specialities.map(speciality => <option value={speciality["@id"]} key={speciality.id}>{speciality.name}</option>)}
                        </select>
					</div>

					<button class="btn btn-primary" type="submit">Submit</button>
					<button class="btn btn-danger" type="reset">Reset</button>
				</form>
				<img src={img2} alt="" class="image-2" />
			</div>
			
		</div>
		
    )
}
export default LevelForm