import React from "react";
import ProgramLine from "./programLine";
import "./../../css/formsStyles/programForm.css";

const ProgramForm = () => {
  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-xs-12 col-sm-10 col-md-9 col-lg-9">
          <form
            className="form row card-body programm-form"
            method="POST"
            action="<?php echo HOST;?>create-program.html"
          >
            <div className="form-group col-12">
              <h4 className="text-primary">Program Form</h4>
              <hr />
            </div>

            <div className="form-group col-12">
              <label for="level">
                Level: <span className="fa fa-leanpub"> </span>
              </label>
              <select name="level[]" className="form-control level">
                <optgroup label="UDS" className="form-control">
                  <option value=""></option>
                </optgroup>
              </select>
            </div>

            <div className="form-group col-12">
              <fieldset className="col-12">
                <legend>
                  Level program{" "}
                  <span className="fa fa-calendar">
                    <hr />
                  </span>
                </legend>

                <table
                  cellspacing="5"
                  cellpadding="5"
                  border="0"
                  className="col-12 table-responsive"
                >
                  <thead>
                    <tr className="col-12">
                      <td className="col-3">
                        <label for="day">
                          Day <span className="fa fa-trello"></span>
                        </label>
                      </td>
                      <td className="col-4">
                        <label for="course">
                          Course <span className="fa fa-book"></span>
                        </label>
                      </td>
                      <td className="col-1">
                        <label for="beginingHour">
                          Begining Hour <span className="fa fa-clock-o"></span>
                        </label>
                      </td>
                      <td className="col-1">
                        <label for="endingHour">
                          Ending Hour <span className="fa fa-clock-o"></span>
                        </label>
                      </td>
                      <td className="col-3">
                        <label for="room">
                          Room <span className="fa fa-home"></span>
                        </label>
                      </td>
                    </tr>
                  </thead>
                  <tbody className="col-12">
                    <ProgramLine />
                    <div id="additionZone">
                      <span className="fa fa-plus-circle"></span> Add Row
                    </div>
                  </tbody>
                </table>
              </fieldset>
            </div>

            <div className="form-group">
              <input
                type="submit"
                value="submit"
                id="submit"
                name="submit"
                className="btn btn-success submit-btn"
              />
              <input
                id="reset"
                type="reset"
                value="reset"
                className="btn btn-info"
              />
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
export default ProgramForm;
