import React from 'react'
import './../../css/style.css'
import './../../fonts/linearicons/style.css'
import img1 from './../../images/image-1.png'
import img2 from './../../images/image-2.png'

const DepartmentForm = () => {
    return (
        <div class="wrapper">
        <div class="inner">
            <img src={img1} class="image-1" />
            <form action="">
                <h3>Department</h3>

                <div class="form-holder">
                    <input type="hidden"  placeholder="First Name" name="id"/>
                </div>

                <div class="form-holder">
                    <span class="lnr lnr-graduation-hat"></span>
                    <input type="text" class="form-control" placeholder="Name" name="name"/>
                </div>

                <div class="form-holder">
                    <span class="lnr lnr-graduation-hat"></span>
                    <input type="text" class="form-control" placeholder="Department" name="Department"/>
                </div>

                <div class="form-holder">
                    <span class="lnr lnr-graduation-hat"></span>
                    <input type="text" class="form-control" placeholder="Faculty" name="Faculty"/>
                </div>


                <button>
                    <span>Register</span>
                </button>
            </form>
            <img src={img2} alt="" class="image-2" />
        </div>
        
    </div>
    )
}

export default DepartmentForm 