import React, {useState} from 'react'
import './../../css/style.css'
import './../../fonts/linearicons/style.css'
import img1 from './../../images/image-1.png'
import img2 from './../../images/image-2.png'
import axios from 'axios'

const InstitutionForm = () => {

	const [institution, setInstitution] = useState({
		name:"",
		localisation:"",
		POBOX:"",
		viceChancellor:""
	})

	const handleChange = ({currentTarget})=>{
        const {name,value} = currentTarget;
        setInstitution({...institution,[name]:value});
    }
	
	const handleSave = (e)=>{
        // e.preventDefault();
        axios.post("https://localhost:8000/api/institutions", institution).then(response =>{ console.log(response) }).catch(error=>console.log(error.response));
	}
	
	console.log(institution);
    return(
        <div class="wrapper">
			<div class="inner">
                <img src={img1} class="image-1" />
				<form action="" onSubmit={handleSave}>
					<h3>Institution</h3>


					<div class="form-holder">
						<span class="lnr lnr-user"></span>
						<input type="text" class="form-control" placeholder="Name" name="name" onChange={handleChange}/>
					</div>

                    <div class="form-holder">
						<span class="lnr lnr-map-marker"></span>
						<input type="text" class="form-control" placeholder="Localisation" name="localisation" onChange={handleChange} />
					</div>
                    
					<div class="form-holder">
						<span class="lnr lnr-envelope"></span>
						<input type="text" class="form-control" placeholder="POBOX" name="POBOX" onChange={handleChange}/>
					</div>
					<div class="form-holder">
						<span class="lnr lnr-graduation-hat"></span>
						<input type="text" class="form-control" placeholder="Head Teacher" name="viceChancellor" onChange={handleChange}/>
					</div>
                    <div class="form-holder">
						<span class="lnr lnr-unlink"></span>
						<input type="url" class="form-control" placeholder="Web Site" name="Website" />
					</div>

					<div class="form-holder">
                    <span class="lnr lnr-picture"></span>
						<input type="file" class="form-control" placeholder="Logo" name="Logo"/>
					</div>
					<button class="btn btn-primary" type="submit">Submit</button>
					<button class="btn btn-danger" type="reset">Reset</button>
				</form>
				<img src={img2} alt="" class="image-2" />
			</div>
			
		</div>
		
    )
}

export default InstitutionForm