import Axios from "axios";
import React, { useState, useEffect } from "react";
import { AddSpeciality } from "../../providers/SpecialityProvider";
import "./../../css/style.css";
import "./../../fonts/linearicons/style.css";
import img1 from "./../../images/image-1.png";
import img2 from "./../../images/image-2.png";

const SpecialityForm = () => {
  const [departments, setDepartments] = useState([]);

  useEffect(() => {
    Axios.get("http://localhost:8000/api/departments")
      .then((response) => {
        setDepartments(response.data["hydra:member"]);
      })
      .catch((error) => console.log(error.response));
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();

    let form = new FormData(e.target);

    AddSpeciality(form)
      .then(() => {
        console.log("C'est validé !");
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return (
    <div class="wrapper">
      <div class="inner">
        <img src={img1} class="image-1" />
        <form onSubmit={handleSubmit}>
          <h3>Speciality</h3>

          <div class="form-holder">
            <input type="hidden" placeholder="First Name" name="id" />
          </div>

          <div class="form-holder">
            <span class="lnr lnr-user"></span>
            <input
              type="text"
              class="form-control"
              placeholder="Name"
              name="Name"
            />
          </div>

          <div class="form-holder">
            <span class="lnr lnr-graduation-hat"></span>
            <select class="browser-default custom-select">
              {departments.map((dep) => (
                <option value={dep["@id"]} key={dep.id}>
                  {dep.name}
                </option>
              ))}
            </select>
          </div>

          <button class="btn btn-primary" type="submit">
            Submit
          </button>
          <button class="btn btn-danger" type="reset">
            Reset
          </button>
        </form>
        <img src={img2} alt="" class="image-2" />
      </div>
    </div>
  );
};

export default SpecialityForm;
