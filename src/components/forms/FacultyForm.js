import React from 'react-dom'
import { AddFaculty } from '../../providers/FacultyProvider'
// import './../../css/formUser.css'
import './../../css/style.css'
import './../../fonts/linearicons/style.css'
import img1 from './../../images/image-1.png'
import img2 from './../../images/image-2.png'


const FacultyForm = () => {

	const handleSubmit = (e) => {
		e.preventDefault();

		let form = new FormData(e.target);

		AddFaculty(form)
			.then(() => {
				console.log("C'est validé !");
			})
			.catch((e) => {
				console.log(e);
			})
	}

	return (
		<div class="wrapper">
			<div class="inner">
				<img src={img1} class="image-1" />
				<form id="formulaire-ajouter-faculte" onSubmit={handleSubmit}>
					<h3>Faculty</h3>

					<div class="form-holder">
						<input type="hidden" name="id" />
					</div>

					<div class="form-holder">
						<span class="lnr lnr-graduation-hat"></span>
						<input type="text" class="form-control" placeholder="Name" name="name" />
					</div>

					<div class="form-holder">
						<span class="lnr lnr-graduation-hat"></span>
						<input type="text" class="form-control" placeholder="Dean Name" name="deanName" />
					</div>

					<div class="form-holder">
						<span class="lnr lnr-envelope"></span>
						<input type="text" class="form-control" placeholder="Web Site" name="webSite" />
					</div>

					<div class="form-holder">
						<span class="lnr lnr-picture"></span>
						<input type="file" class="form-control" placeholder="Logo" name="logo" />
					</div>
					<button type="submit">
						<span>Register</span>
					</button>
				</form>
				<img src={img2} alt="" class="image-2" />
			</div>

		</div>

	)
}

export default FacultyForm;
