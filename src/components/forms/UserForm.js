import React from 'react-dom'
// import './../../css/formUser.css'
import './../../css/style.css'
import './../../fonts/linearicons/style.css'
import img1 from './../../images/image-1.png'
import img2 from './../../images/image-2.png'


const UserForm = () =>{
    return(
        <div class="wrapper">
			<div class="inner">
                <img src={img1} class="image-1" />
				<form action="">
					<h3>New User ?</h3>

                    <div class="form-holder">
						<input type="hidden"  placeholder="First Name" name="id"/>
					</div>

					<div class="form-holder">
						<span class="lnr lnr-user"></span>
						<input type="text" class="form-control" placeholder="First Name" name="FirstName"/>
					</div>

                    <div class="form-holder">
						<span class="lnr lnr-user"></span>
						<input type="text" class="form-control" placeholder="Given Name" name="GivenName" />
					</div>
                    
					<div class="form-holder">
						<span class="lnr lnr-envelope"></span>
						<input type="text" class="form-control" placeholder="Login" name="Login" />
					</div>
					<div class="form-holder">
						<span class="lnr lnr-lock"></span>
						<input type="password" class="form-control" placeholder="Password" name="Password"/>
					</div>
                    <div class="form-holder">
						<span class="lnr lnr-lock"></span>
						<input type="password" class="form-control" placeholder="Confirm Password" name="Password" />
					</div>

					<div class="form-holder">
                    <span class="lnr lnr-picture"></span>
						<input type="file" class="form-control" placeholder="Image" name="Image"/>
					</div>
					<button>
						<span>Register</span>
					</button>
				</form>
				<img src={img2} alt="" class="image-2" />
			</div>
			
		</div>
		
    )
}

export default UserForm;
