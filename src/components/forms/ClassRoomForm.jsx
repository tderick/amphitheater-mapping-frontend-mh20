import React,{useState, useEffect} from 'react'
// import './../../css/formUser.css'
import './../../css/style.css'
import './../../fonts/linearicons/style.css'
import img1 from './../../images/image-1.png'
import img2 from './../../images/image-2.png'
import axios from 'axios'

const ClassRoomForm = () => {

    const [campus, setCampus]= useState([])
    const [classroom, setClassroom] = useState({
        name:"",
        campus:""
    })

    useEffect(() => {
       axios.get("https://localhost:8000/api/campuses").then(response =>{ setCampus(response.data['hydra:member']) }).catch(error=>console.log(error.response));
    }, []);

    const handleChange = ({currentTarget})=>{
        const {name,value} = currentTarget;
        setClassroom({...classroom,[name]:value});
    }

    const handleSave = (e)=>{
        // e.preventDefault();
        axios.post("https://localhost:8000/api/classrooms", classroom).then(response =>{ console.log(response.data['hydra:member']) }).catch(error=>console.log(error.response));
    }

    console.log(classroom);
    return (
        <div class="wrapper">
        <div class="inner">
            <img src={img1} class="image-1" />
            <form action="" onSubmit={handleSave}>
                <h3>Classroom</h3>

                <div class="form-holder">
                    <span class="lnr lnr-graduation-hat"></span>
                    <input type="text" class="form-control" placeholder="Name" name="name" onChange={handleChange}/>
                </div>

                <div class="form-holder">
                    <span class="lnr lnr-graduation-hat"></span>
                  
                    <select class="browser-default custom-select" onChange={handleChange} name="campus">
                        <option selected>  Campus Membership</option>
                        {campus.map(camp => <option value={camp["@id"]} key={camp.id} >{camp.name}</option>
                        )}
                    </select>
                </div>

                <div class="form-holder">
                    {/* <span class="lnr lnr-user"></span> */}
                    <input type="number" class="form-control" placeholder="capacity" name="capacity" />
                </div>
                
               
                <div class="form-holder">
                <span class="lnr lnr-picture"></span>
                    <input type="file" class="form-control" placeholder="Image" name="Image"/>
                </div>
                <button type="submit">
                    <span>Register</span>
                </button>
            </form>
            <img src={img2} alt="" class="image-2" />
        </div>
        
    </div>
    )
}

export default ClassRoomForm