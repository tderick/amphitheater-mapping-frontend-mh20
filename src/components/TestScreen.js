import React, { useState, useEffect } from 'react'
import { getAll, getAllCampuses } from '../providers/CampusProvider';
import { AddFaculty } from '../providers/FacultyProvider';

const TestScreen = () => {

    const [campus, setCampus] = useState(null);

    useEffect(() => {

        AddFaculty()
        .then((res) => {
            console.log(res);
        })
        .catch(reason => {
            console.log(reason);
        })
        
        return () => {
            
        }
    }, [])

    return (
        <div>

        </div>
    )
}

export default TestScreen
