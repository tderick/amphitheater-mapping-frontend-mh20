import React from 'react';

const ClassCard = ({ classe }) => {
    return (
        <div className="col-xs-12 col-sm-4 col-md-3 col-lg-2">
            <div className="OneClass-ordre ">
                <h6 className="OneClass-footer-daryl"></h6>
                <h6 className="OneClass-header">{classe.name}</h6>
                <div className="OneClass">
                    <span class="fa plus-circle"></span>
                    <span class="fa fa-recycle"></span>
                    <span class="fa fa-map-marker"></span>
                    <span class="fa fa-trash"></span>
                </div>
            </div>
        </div>
    )
}

export default ClassCard;