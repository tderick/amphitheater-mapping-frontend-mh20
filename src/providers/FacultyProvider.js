import Axios from "axios";

export const getAllFaculties = async () => {
    let result = {};

    await fetch("http://localhost:8000/api/faculties", { method: "GET"})
        .then(res => res.json())
        .then((res) => {
            result = res;
        })

    return result;
}

export const AddFaculty = async (donnees) => {
    let result = {};

    let form = donnees;
    form.append("institution", "/api/institutions/1");

    let valeurs = {};

    form.forEach((value, key) => {
        if (key != 'id') valeurs[key] = value;
    })

    await fetch("http://localhost:8000/api/faculties", { method: "POST", body: JSON.stringify(valeurs), headers: new Headers({ 'content-type': "application/json" }) })
        .then((res) => res.json())
        .then((val) => {
            console.log(val);
        })
        .catch(reason => {
            console.log(reason);
        })

    return result;
}