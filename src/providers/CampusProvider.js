import HttpServices from "../services/HttpServices";

export const getAllCampuses = async () => {
    let result = {};

    await HttpServices('GET', '/campuses')
        .then((res) => {
            result = res;
        })

    return result;
}
