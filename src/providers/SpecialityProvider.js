export const AddSpeciality = async (donnees) => {
    let result = {};

    let form = donnees;
    form.append("department", "/api/departments/1");

    let valeurs = {};

    form.forEach((value, key) => {
        if (key != 'id') valeurs[key] = value;
    })

    await fetch("http://localhost:8000/api/specialities", { method: "POST", body: JSON.stringify(valeurs), headers: new Headers({ 'content-type': "application/json" }) })
        .then((res) => res.json())
        .then((val) => {
            console.log(val);
        })
        .catch(reason => {
            console.log(reason);
        })

    return result;
}